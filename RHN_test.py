import numpy as np
from components.RHN import RHN
from components.Optimizer import SPSA


memory = np.array([[0, 0, 0],
                  [0, 1, 1],
                  [1, 0, 1]]).T
test = np.array([[0, 0, 0],
                 [0, 1, 1],
                 [1, 0, 1],
                 [0, 1, 0],
                 [1, 0, 0]]).T
hidden_num = 4

test_iter = 1000
result = []
for i in range(test_iter):
    # Compute cost of MLP & create the optimizer class
    opt_iter = 10000
    optimizer = SPSA(a=100.0, c=2.0, A=opt_iter/10.0, alpha=1.0, gamma=0.167)
    # create the linear model
    rhn = RHN(hidden_num=hidden_num, memory=memory,optimizer=optimizer, num_iterations=opt_iter)
    rhn.RHN_model()
    result.append(rhn.predict(test))
acc = np.sum(result, axis=0) / float(test_iter)
print('Accuracy: ' + str(acc))