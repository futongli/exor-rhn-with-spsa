import numpy as np
from components.Optimizer import SPSA
from components.Helper import initialize_parameters, match_memory


class RHN:
    def __init__(self, hidden_num, memory, optimizer, num_iterations,reg_factor=0.0):
        self.hidden_num = hidden_num
        self.memory = memory
        self.optimizer = optimizer
        self.reg_factor = reg_factor
        self.num_iterations = num_iterations

        # Parameters initialization
        self.parameters = initialize_parameters(self.hidden_num, self.memory.shape[0])

    def RHN_model(self, print_loss=False):
        for iters in range(0, self.num_iterations):
            # Print the cost every 1000 training example
            # Here, the cost is the minimal one overall
            m = self.memory.shape[1]  # the number of training data
            # Calculate the forward and backward propagation
            AL = match_memory(self.memory, self.parameters)
            loss = np.zeros(m)
            for j in range(m):
                # Obtain the minimal loss for each input according to each pattern
                loss[j] = np.min(np.sum((self.memory - AL[:, j]) ** 2, axis=0))
                # Print the minimal loss among all the patterns
                if print_loss and iters % 100 == 0:
                    print ("Loss after iteration %i: %f" % (iters, np.min(loss)))
            # Update the weight
            self.parameters = self.optimizer.step(current_estimate=self.parameters, data=self.memory)

    def predict(self, test_data, annealing=1):
        m = test_data.shape[1]  # the number of inputs
        data_out = np.zeros(test_data.shape)
        # Forward propagation        
        probas = match_memory(test_data, self.parameters, annealing)
        # convert probas to 0/1 predictions
        for i in range(probas.shape[0]):
            for j in range(probas.shape[1]):
                if probas[i, j] > 0.5:
                    data_out[i, j] = 1
                else:
                    data_out[i, j] = 0
        # print results
        print ("predictions: \n" + str(data_out.T))
        '''
        for i in range(len(self.parameters)):
            print(self.parameters[i])
        
        acc = np.sum(data_out == self.memory, axis=0) / m
        return acc
        '''

