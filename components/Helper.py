import numpy as np


def initialize_parameters(hidden_num, visible_num):
    np.random.seed()
    L = 5  # number of layers in the nn
    '''
    parameters: W[0]b[0]W[1]b[1]W[2]b[2]W[3]b[3]
    # W := (the # of nodes of layer l) * (the # of nodes of layer l+1)
    # b := the # of nodes in layer l
    # the even numbers of parameters are the wights, and the odd numbers of parameters are biases
    '''
    parameters = []
    W = np.random.randn(hidden_num, visible_num) * 0.01
    for l in range((L-1)/2):
        parameters.append(W)
        parameters.append(np.zeros((hidden_num, 1)))
        parameters.append(W.T)
        parameters.append(np.zeros((visible_num, 1)))
    return np.array(parameters)


def sigmoid(Z, annealing):
    A = 1/(1+np.exp(-Z / annealing))
    return A


def propagation(A, W, b, annealing):
    Z = np.dot(W, A) + b
    A = sigmoid(Z, annealing)
    return A


def match_memory(X, parameters, annealing=1):
    A = X
    L = len(parameters) / 2  # layers in the neural network
    for l in range(0, L, 2):
        A = propagation(A, parameters[l], parameters[l + 1], annealing)
    return A