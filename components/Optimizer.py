import numpy as np
from components.Helper import match_memory


class SPSA:
    """
    An optimizer class that implements Simultaneous Perturbation Stochastic Approximation (SPSA)
    """
    def __init__(self, a, c, A, alpha, gamma):
        # Initialize gain parameters and decay factors
        self.a = a
        self.c = c
        self.A = A
        self.alpha = alpha
        self.gamma = gamma

        self.min_vals = -4.0
        self.max_vals = 4.0

        # counters
        self.t = 0

    def step(self, current_estimate, data):
        """
        :param data: memory data
        :param current_estimate: This is the current estimate of the parameter vector
        :return: returns the updated estimate of the vector
        """
        m = data.shape[1]  # the number of training data
        # get the current values for gain sequences
        a_t = self.a / (self.t + 1 + self.A) ** self.alpha
        c_t = self.c / (self.t + 1) ** self.gamma

        # Measure the loss function at perturbations
        # get the random perturbation vector from bernoulli distribution which has to be symmetric around zero
        # But normal distribution does not work which makes the perturbations close to zero
        # Also, uniform distribution should not be used since they are not around zero
        L = 5  # the number of layers
        delta_w = np.random.randint(0, 2, current_estimate[0].shape) * 2 - 1  # delta for weights
        deltas = []  # Store corresponding delta for every pair of weights and biases
        for l in range((L-1)/2):
            deltas.append(delta_w)
            deltas.append(np.random.randint(0, 2, current_estimate[1].shape) * 2 - 1)
            deltas.append(delta_w.T)
            deltas.append(np.random.randint(0, 2, current_estimate[3].shape) * 2 - 1)
        # Compute J(w+) and J(w-)
        current_estimate_plus = []
        current_estimate_minus = []
        for l in range(len(current_estimate)):
            current_estimate_plus.append(current_estimate[l] + deltas[l] * c_t)
            current_estimate_minus.append(current_estimate[l] - deltas[l] * c_t)
        J_plus = np.sum((data - match_memory(data, current_estimate_plus)) ** 2) / m
        J_minus = np.sum((data - match_memory(data, current_estimate_minus)) ** 2) / m

        # Update the estimate of the parameters using the minimum of the loss
        loss_cal = np.min(J_plus - J_minus)
        for l in range(len(current_estimate)):
            # compute the estimate of the gradient
            g_t = loss_cal / (2.0 * deltas[l] * c_t)
            current_estimate[l] = current_estimate[l] - a_t * g_t
            # Ignore results that are outside the boundaries
            if (self.min_vals is not None) and (self.max_vals is not None):
                current_estimate[l] = np.minimum(current_estimate[l], self.max_vals)
                current_estimate[l] = np.maximum(current_estimate[l], self.min_vals)

        # increment the counter
        self.t += 1

        return current_estimate



